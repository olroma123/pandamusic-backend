# API documentation of PandaMusic service
Provide the documentation of api service PandaMusic
## v1
### Structure
Endpoint
```
POST http://localhost:8080/api/pm/v1/
```

Default body
```javascript
{
  key: '<API_KEY>',
  method: 'ROUTE#METHOD_NAME',
  param: JSON.stringify({}),
}
```

Response template
```javascript
{
  message: {
    type: '',// Type of message (SUCCESS | ERROR | WARNING)
    message: '', // Text of message
  },
  data: {} // Payload of response
}
```

Default codes
1. 200 - Ok
2. 400 - Required params is not transfered
3. 403 - Wrong user's credits (e.g. token)
4. 520 - Server error

Field `method` has simple syntax
```
Route#Method
```

There is 6 routes in api
1. Admin - provide an admin functions
2. Security - provide an authentication methods
3. User - provide user methods
4. Track - provide a track's methods
5. Hash - provide a hash functionality
6. Playlist - provide a crud on playlist

### **Routes**
#### Admin
- **Admin#GetUsers** - Get information about user's or only specific user by id  
  **Body**
  ```javascript
  {
    key: // required
    method: 'Admin#GetUsers',
    param: JSON.stringify({id: /* optional */})
  } 
  ```

  **Result (array or single object)**
  ```
  [
    {
      id: (int),
      login: (string),
      name: {
        name: (string),
        surname: (string)
      },
      email: (string)
      permission: {
        name: (string)
      }
    }
  ]
  ```

  **Codes**
  1. 200 - ok
  2. 204 - no user or user's in database
  3. 520 - server error

- **Admin#DeleteAccount** - Delete specific account by ID  
  **Body**
  ```javascript
  {
    key: // required
    method: 'Admin#DeleteAccount',
    param: JSON.stringify({id: /* required */})
  } 
  ```
   **Codes**
  1. 200 - ok
  3. 520 - server error

#### Security
- **Security#Auth** - Primary authorize with login and password  
  **Body**
  ```javascript
  {
    key: ''
    method: 'Security#Auth',
    param: JSON.stringify({
      login: ,  /* required */
      password: /* required */
    })
  } 
  ```

  **Result**
  ```
  {
    id: (int),
    login: (string),
    token: (string),
    avatar: (string),
    name: {
      name: (string),
      surname: (string)
    },
    email: (string),
    permission: {
      name: (string),
      description: (string)
    },
    setting: {
      volume: (float),
      equalizer: {
        enabled: (boolean),
        band: [
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float)
        ]
      }
    }
  }
  ```

  **Codes**
  1. 200 - ok
  2. 400 - Parameters' required
  3. 403 - Wrong login or password
  3. 520 - server error

- **Security#AuthGoogle** - Authorize with google account. Also that create an account if it doesn't exists. And also that make link to google account if it exists  
  **Body**
  ```javascript
  {
    key: ''
    method: 'Security#AuthGoogle',
    param: JSON.stringify({
      token: ,  /* required */
    })
  }

  /* 
    token - id_token from Google API 
  */
  ```

  **Result**
  ```
  {
    id: (int),
    login: (string),
    token: (string),
    avatar: (string),
    name: {
      name: (string),
      surname: (string)
    },
    email: (string),
    permission: {
      name: (string),
      description: (string)
    },
    setting: {
      volume: (float),
      equalizer: {
        enabled: (boolean),
        band: [
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float),
          (float)
        ]
      }
    }
  }
  ```

  **Codes**
  1. 200 - ok
  2. 400 - Parameters' required
  3. 403 - When E-mail AND GoogleID exists, but ID from google token is not right 
  3. 520 - server error

- **Security#IsUserHaveRights** - Return boolean result that represent is user have right's by name of role  
  **Body**
  ```javascript
  {
    key: /* required */
    method: 'Security#IsUserHaveRights',
    param: JSON.stringify({
      roleName: ,  /* required */
    })
  }

  /* 
    roleName - name of role.
  */
  ```

  **Result**
  ```
  {
    isUserHaveRights: (boolean)
  }
  ```

  **Codes**
  1. 200 - ok
  2. 400 - Parameters' required
  3. 403 - Wrong token
  3. 520 - server error

- **Security#Register** - Register an account  
  **Body**
  ```javascript
  {
    key: ''
    method: 'Security#Register',
    param: JSON.stringify({
      login: ,      /* required */
      password: ,   /* required */
      email: ,      /* required */
    })
  }
  ```

  **Result**
  ```
  []
  ```

  **Codes**
  1. 200 - ok
  2. 400 - No required params or wrong format
  3. 409 - Account already exists (login or email)
  4. 520 - server error

#### Track
  - **Track#GetTracks** - Get cached information of uploaded track's
  **Body**
  ```javascript
  {
    key: // required
    method: 'Track#GetTracks',
    param: '{}'
  }
  ```

  **Result**
  ```
  {
    tracks: [
      {
        bitrate: (int),
        duration: (float),
        artist: (string),
        title: (string),
        album: (string),
        hash: (string),
        thumbnail: (string),
        fileUrl: (string)
      }
    ],

    albums: [
      {
        name: (string),
        index: (array[int])
      }
    ]
  }
  ```

  **Codes**
  1. 200 - ok
  2. 403 - Wrong token
  3. 520 - server error

- **Track#UploadTrack** - Upload single (or multiple) track
  **Body**
  ```javascript
  {
    key: // required
    method: 'Track#UploadTrack',
    param: '{}',
    track: FormData
  }
  ```

  **Result**
  ```
  [
    {
      status: (string)
      musicData: {
        bitrate: (int),
        duration: (float),
        artist: (string),
        title: (string),
        album: (string),
        hash: (string),
        fileName: (string),
        isDefaultThumbnail: (boolean)
      }
    }
  ]
  ```

  **Codes**
  1. 200 - ok
  2. 400 - No required params
  3. 403 - Wrong token
  4. 520 - server error

#### Playlist
- **Playlist#CreatePlaylist** - Create custom playlist
  **Body**
  ```javascript
  {
    key: // required
    method: 'Playlist#CreatePlaylist',
    param: JSON.stringify({
      name: ,       // name of new playlist
      description:  // description of new playlist
    })
  }
  ```

  **Result**
  ```
  []
  ```

  **Codes**
  1. 200 - ok
  2. 400 - No required params
  3. 403 - Wrong token
  4. 520 - server error

- **Playlist#GetPlaylist** - Get user's custom playlist's
  **Body**
  ```javascript
  {
    key: // required
    method: 'Playlist#GetPlaylist',
    param: '{}'
  }
  ```

  **Result**
  ```
  [
    {
      name: (string),
      description: (string),
      cover: (string),
      songs: (array[string]), // here is array with hashes
      id: (string),
      owner: (string)
    }
  ]
  ```

  **Codes**
  1. 200 - ok
  2. 204 - empty plyalist
  3. 400 - No required params
  4. 403 - Wrong token
  5. 520 - server error

- **Playlist#AddSong** - Add song (hash) to custom playlist by id
  **Body**
  ```javascript
  {
    key: // required
    method: 'Playlist#AddSong',
    param: JSON.stringify({
      playlist: ,   // id of playlist
      hash :        // song's hash
    })
  }
  ```

  **Result**
  ```
  []
  ```

  **Codes**
  1. 200 - ok
  2. 400 - No required params
  3. 403 - Wrong token
  4. 412 - Id or hash is not exists in user's folder
  5. 520 - server error
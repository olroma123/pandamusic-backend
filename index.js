const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const fileUploadMiddleware = require('express-fileupload');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/avatar', express.static(__dirname + '/app/assets/avatars'));
app.use('/users', express.static(__dirname + '/app/users'));
app.use('/assets', express.static(__dirname + '/app/assets'));
app.use('/documentation/v1', express.static(__dirname + '/app/html/PandaMusic API.html'));
app.use(fileUploadMiddleware());
app.use(helmet());

// Initialize library-
const lib = require('./app/lib');
lib.db = lib.db(lib);
lib.helper = lib.helper(lib);

// Initialize api gateway
const APIGatewayV1 = require('./app/gateway')(app, lib);

// Default route to v1 api
app.post('/api/pm/v1', (req, res) => {
	res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
	res.append('Content-Type', 'application/json');
	
	APIGatewayV1.newRequest(req, res);
});

// Initalize database connection
lib.db.connect().then(() => {
	lib.logger.info('[PM-Server] Connection to database estabilished');

	// Start server
	app.listen(8080, () => {
		lib.logger.info('[PM-Server] Server start listen on 8080 port');
	});
}).catch(err => { 
	lib.logger.error(err);
})

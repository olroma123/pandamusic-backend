const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);

describe('Тестирование APIGateway v1', () => {
	describe('Валидация параметров', () => {
		it('Переданы все параметры', done => {
      chai.request('http://localhost:8080')
        .post('/api/pm/v1')
        .type('form')
        .send({
          key: '',
          method: 'Security#Auth',
          param: JSON.stringify({
            login: 'OlegRom4ig',
            password: 'morozov'
          })
        })
        .end((err, res) => {
          res.body.message.type.should.equal('SUCCESS');
          res.body.data.login.should.equal('OlegRom4ig');
          done();
        });
		});
		
		describe('Валидация params', () => {
      it('Неправильный формат params', done => {

      });
		});
		
		describe('Проверка на API ключ', () => {
			it('Неправильный ключ', done => {
			
			});
			
			it('Валидный ключ', done => {
			
			});
			
			it('Вызов метода, который не требует ключ', done => {
			
			});
		});
		
		describe('Проверка названия вызываемого метода', () => {
			it('Вызов метода авторизации', done => {
				chai.request('http://localhost:8080')
          .post('/api/pm/v1')
          .type('form')
          .send({
            key: '',
            method: 'Security#Auth',
            param: JSON.stringify({
              login: 'OlegRom4ig',
              password: 'morozov'
            })
          })
          .end((err, res) => {
            res.body.message.type.should.equal('SUCCESS');
            res.body.data.login.should.equal('OlegRom4ig');
            done();
          });
			});
			
			it('Передан неправильный формат метода', done => {
        chai.request('http://localhost:8080')
          .post('/api/pm/v1')
          .type('form')
          .send({
            key: '',
            method: 'Security-Auth',
            param: JSON.stringify({})
          })
          .end((err, res) => {
            res.body.message.type.should.equal('ERROR');
            done();
          });
			});
			
			it('Неправильное название роута', done => {
        chai.request('http://localhost:8080')
          .post('/api/pm/v1')
          .type('form')
          .send({
            key: '',
            method: 'asd#Auth',
            param: JSON.stringify({})
          })
          .end((err, res) => {
            res.body.message.type.should.equal('ERROR');
            done();
          });
			});
			
			it('Неправильное название метода', done => {
        chai.request('http://localhost:8080')
          .post('/api/pm/v1')
          .type('form')
          .send({
            key: '',
            method: 'Security#asd',
            param: JSON.stringify({})
          })
          .end((err, res) => {
            res.body.message.type.should.equal('ERROR');
            done();
          });
			});
		});
	});
});
const creator = require('./creator');

class ERROR {
  static PERMISSION_DENIED() {
    return creator.createErrorMessage('Вы не обладаете нужными правами для доступа к этой функии');
  }

  static WRONG_FORMAT_PARAMS() {
    return creator.createErrorMessage('Неправильный формат данных был передан');
  }

  static WRONG_PARAMS() {
    return creator.createErrorMessage('Не все параметры были переданы');
  }

  static WRONG_TOKEN() {
    return creator.createErrorMessage('Вы не авторизованы');
  }

  static WRONG_ROLE_NAME() {
    return creator.createErrorMessage('Неправильное название роли');
  }

  static SERVER_ERROR(err = undefined) {
    return creator.createErrorMessage('Произошла ошибка на стороне сервера!' + (err ? `Текст ошибки: ${err}` : ''));
  }

  static CANNOT_CHANGE_ROLE_ITSELF() {
    return creator.createErrorMessage('Невозможно изменить роль самому себе');
  }
  
  static WRONG_METHOD_NAME() {
    return creator.createErrorMessage(`Вызываемый метод не найден`);
  }

  static WRONG_METHOD_FORMAT() {
    return creator.createErrorMessage(`Неверный формат вызываемого метода`);
  }

  static WRONG_ROUTE() {
    return creator.createErrorMessage(`Метод не найден`);
  }

  static AUTH_GOOGLEID_IS_NOT_RIGHT() {
    return creator.createErrorMessage(`Данный аккаунт привязан к другому Google аккаунту`);
  }

  static CANNOT_FIND_USER_ID() {
    return creator.createErrorMessage(`Пользователь с таким ID не был найден`);
  }

  static REGISTER_WRONG_EMAIL() {
    return creator.createErrorMessage(`Неверный формат E-mail!`);
  }

  static REGISTER_WRONG_LOGIN() {
    return creator.createErrorMessage(`Неверный формат логина!`);
  }

  static REGISTER_WRONG_CYRILIC() {
    return creator.createErrorMessage(`Неверный формат имени!`);
  }

  static REGISTER_USER_EXISTS() {
    return creator.createErrorMessage(`Пользователь с таким логином или почтой уже существует!`);
  }

  static WRONG_USER_CREDITS() {
    return creator.createErrorMessage(`Неправильный логин или пароль!`);
  }

  static PLAYLIST_ID_IS_NOT_FOUND() {
    return creator.createErrorMessage(`ID плейлиста не был найден`);
  }

  static SONG_HASH_IS_NOT_FOUND() {
    return creator.createErrorMessage(`Данный трек не был найден`);
  }
}

module.exports = ERROR;

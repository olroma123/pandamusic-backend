class CREATOR {
  static createMessage(type, message, data = []) {
    return JSON.stringify({
      message: {
        type: type,
        message: message
      },
      data: data
    });
  }

  static createSuccessMessage(message, data = []) {
    return this.createMessage('SUCCESS', message, data);
  }

  static createErrorMessage(message, data = []) {
    return this.createMessage('ERROR', message, data);
  }

  static createWarningMessage(message, data = []) {
    return this.createMessage('WARNING', message, data);
  }
}

module.exports = CREATOR;

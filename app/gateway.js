class APIGateway {
  constructor(app, lib) {
    this.lib = lib;
    this.app = app;
    this.apiEngine = require('./api/v1')(lib);
  }
  
  /**
  *	Initialize new request from user
  * There is just gateway for checking required params
  */
  newRequest(request, response) {
    /**
    * Method for checking a required params (e.g. api-params, api-method and api-key)
    * @return { boolean } Returns true if required params is exists
    */
    const checkRequireParams = (params) => {
      return !(!params || params.param == 'undefined' || params.key == 'undefined' || !params.method);
    };
      
    const params = request.body;

    // If we have a wrong api request
    // Then send an error and stop execute
    if (!checkRequireParams(params)) {
      response.send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }
	
    this.lib.logger.info(`[PM-Gateway] Call an api method with name: ${params.method}`);

    let method = this.apiEngine.parseMethod(params.method), parsedParam = this.lib.helper.parser.tryParseJSON(params.param);
    if (method.error) {
      this.lib.logger.error(`[PM-Gateway] Error on parse methods: ${method.message.message}`);
      response.send(method.message);
      return;
    } else if (!parsedParam) {
      this.lib.logger.error(`[PM-Gateway] Error on parse param: ${params.param}`);
      response.send(this.lib.apiMessage.ERROR.WRONG_FORMAT_PARAMS());
      return;
    }

    params.method = method;
    params.param = parsedParam;
    this.apiEngine.callMethod(response, params, request);
  }
}

module.exports = (app, lib) => {
	return new APIGateway(app, lib);
};

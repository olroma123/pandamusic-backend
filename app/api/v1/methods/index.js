module.exports = (lib) => {
  return {
    security: require('./security')(lib),
    track: require('./track')(lib),
    playlist: require('./playlist')(lib),
    admin: require('./admin')(lib),
    user: require('./user')(lib)
  }
};
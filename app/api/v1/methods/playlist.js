class APIPlaylist {
  constructor(lib) {
    this.lib = lib;
  }
  
  addPlaylist(data, response) {
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['name', 'description'], data.param)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      // read current user's playlist from file
      const playlistPath = `${this.lib.helper.path.getUserFolderPath(user.Username)}/playlist.json`;
      this.lib.fs.readFile(playlistPath, 'utf-8', (err, playlistData) => {
        if (err) {
          this.lib.logger.error(`[PM-API] An error occured on reading ${user.Username} playlist: ${err}`);
          return response.status(400).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
        }

        // Push new information about playlist to array of custom's playlist
        playlistData = JSON.parse(playlistData);
        playlistData.custom.push({
          name: data.param.name,
          description: data.param.description,
          cover: {
            isDefault: true
          },
          songs: [],
          id: this.lib.uuid()
        });

        // write new info to file
        this.lib.fs.writeFile(playlistPath, JSON.stringify(playlistData), (err) => {
          if (err) {
            this.lib.logger.error(`[PM-API] An error occured on reading ${user.Username} playlist: ${err}`);
            return response.status(400).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
          }

          response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE());
        });
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for adding a song to custom playlist
   */
  addSongToPlaylist(data, response) {
    const { param } = data;
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['hash', 'playlist'], param)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      // read playlist's file
      this.lib.fs.readFile(`${this.lib.helper.path.getUserFolderPath(user.Username)}/playlist.json`, 'utf-8', (err, data) => {
        if (err) {
          this.lib.logger.error(`[PM-API] An error occured on reading ${user.Username} playlist (addSongToPlaylist): ${err}`);
          return response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
        }

        data = JSON.parse(data);
        // Search playlist in list
        // And if playlist id is not in user's playlist
        // Than just send an error
        let playlistIndex = data.custom.findIndex(playlist => playlist.id === param.playlist);
        if (playlistIndex === -1) return response.status(412).send(this.lib.apiMessage.ERROR.PLAYLIST_ID_IS_NOT_FOUND());

        // search hash in playlist
        // If we already have hash inside array
        // Than just send a message with SUCCESS status 
        let hashIndex = data.custom[playlistIndex].songs.findIndex(song => song === param.hash);
        if (hashIndex !== -1) return response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE());

        // Check track to exists by hash name
        this.lib.fs.stat(`${this.lib.helper.path.getUserCacheFolderPath(user.Username)}/${param.hash}.json`, (err, stats) => {
          if (err) {
            if (err.code === 'ENOENT')
              return response.status(412).send(this.lib.apiMessage.ERROR.SONG_HASH_IS_NOT_FOUND());
            else {
              this.lib.logger.error(`[PM-API] An error occured on checking ${param.hash} track (addSongToPlaylist): ${err}`);
              return response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
            }
          }

          // update playlist file
          data.custom[playlistIndex].songs.push(param.hash);
          this.lib.fs.writeFile(`${this.lib.helper.path.getUserFolderPath(user.Username)}/playlist.json`, JSON.stringify(data), (err) => {
            if (err) {
              this.lib.logger.error(`[PM-API] An error occured on writing ${user.Username} playlist: ${err}`);
              return response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
            }

            response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE());
          });
        });
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    })
  }

  /**
   * Method for getting information of user's playlist
   */
  getPlaylistsInformation(data, response) {
    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      const playlistPath = `${this.lib.helper.path.getUserFolderPath(user.Username)}/playlist.json`;
      this.lib.fs.readFile(playlistPath, 'utf-8', (err, data) => {
        if (err) {
          this.lib.logger.error(`[PM-API] An error occured on reading ${user.Username} playlist (getPlaylistsInformation): ${err}`);
          return response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
        }

        data = JSON.parse(data);

        if (data.custom.length === 0)
          return response.status(204).send();

        for (let i = 0; i < data.custom.length; i++) {
          data.custom[i].owner = user.Username;
          data.custom[i].cover = this.lib.helper.url.getThumbnailDefault();
        }

        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(data.custom));
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = (lib) => new APIPlaylist(lib);

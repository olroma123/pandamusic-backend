module.exports = (lib) => {
  const APIEngine = require('./engine')(lib);

  return APIEngine;
};
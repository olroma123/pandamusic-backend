class Routes {
  constructor(lib) {
    this.lib = lib;
    this.methods = require('./methods')(lib);
  }

  SecurityRoute(methodName, response, data, request) {
    switch (methodName) {
      case 'Auth': {
        this.methods.security.authorizeByLoginAndPassword(data, response);
        break;
      }

      case 'AuthGoogle': {
        this.methods.security.authByGoogleApi(data, response);
        break;
      }

      case 'IsUserHaveRights': {
        this.methods.security.isUserIsHaveRightsByRoleName(data, response);
        break;
      }

      case 'Register': {
        this.methods.security.registerAccount(data, response);
        break;
      }

      default: {
        response.send(this.lib.apiMessage.ERROR.WRONG_METHOD_NAME());
        break;
      }
    }
  }

  TrackRoute(methodName, response, data, request) {
    switch (methodName) {
      case 'GetTracks': {
        this.methods.track.getMusicCache(data, response);
        break;
      }

      case 'UploadTrack': {
        this.methods.track.uploadTracks(request, response, data);
        break;
      }

      default: {
        response.send(this.lib.apiMessage.ERROR.WRONG_METHOD_NAME());
        break;
      }
    }
  }

  PlaylistRoute(methodName, response, data, request) {
    switch (methodName) {
      case 'CreatePlaylist': {
        this.methods.playlist.addPlaylist(data, response);
        break;
      }

      case 'GetPlaylist': {
        this.methods.playlist.getPlaylistsInformation(data, response);
        break;
      }

      case 'AddSong': {
        this.methods.playlist.addSongToPlaylist(data, response);
        break;
      }

      default: {
        response.send(this.lib.apiMessage.ERROR.WRONG_METHOD_NAME());
        break;
      }
    }
  }

  AdminRoute(methodName, response, data, request, adminInfo) {
    switch (methodName) {
      case 'GetUsers': {
        this.methods.admin.getUsersInformation(data, response, adminInfo);
        break;
      }

      case 'ChangeUserRole': {
        this.methods.admin.changeUsersRole(data, response, adminInfo);
        break;
      }

      case 'DeleteAccount': {
        this.methods.admin.deleteAccount(data, response, adminInfo);
        break;
      }

      default: {
        response.send(this.lib.apiMessage.ERROR.WRONG_METHOD_NAME());
        break;
      }
    }
  }

  UserRoute(methodName, response, data, request) {
    switch (methodName) {
      case 'ChangePassword': {
        this.methods.user.updateUserPassword(data, response);
        break;
      }

      default: {
        response.send(this.lib.apiMessage.ERROR.WRONG_METHOD_NAME());
        break;
      }
    }
  }
}

module.exports = (lib) => new Routes(lib);
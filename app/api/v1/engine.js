class APIEngine {
	constructor(lib) {
		this.lib = lib;
		this.routes = require('./routes')(lib);
	}

	/**
	 * Method for parse a method syntax
	 * @param { string } method A method name with syntax: ROUTE_NAME#METHOD_NAME
	 * @returns { object } Return an object with method name and route name
	*/
	parseMethod(method) {
		let methodSplited = method.split('#');
		if (methodSplited.lenght < 2 || methodSplited > 2) return {error: true, message: this.lib.apiMessage.ERROR.WRONG_METHOD_FORMAT()};

		return {
			method: methodSplited[1],
			route: methodSplited[0]
		}
	}
	
	/**
	 * Method for call a method by name route and method
	 * @param { object } data API params
	*/
	callMethod(response, data, request) {
		const { route, method } = data.method;
		switch (route.toUpperCase()) {
			case 'SECURITY': {
				this.routes.SecurityRoute(method, response, data, request);
				break;
			}

			case 'PLAYLIST': {
				this.routes.PlaylistRoute(method, response, data, request);
				break;
			}

			case 'TRACK': {
				this.routes.TrackRoute(method, response, data, request);
				break;
			}

			case 'ADMIN': {
				this.lib.helper.schema.getUserByTokenAndValidateByRole(data.key, 1).then(user => {
					this.routes.AdminRoute(method, response, data, request, user);
				}).catch(err => {
					response.status(err.status || 520).send(err.message);
				});

				break;
			}

			default: {
				response.send(this.lib.apiMessage.ERROR.WRONG_ROUTE());
				break;
			}
		}
	}
}

module.exports = (lib) => new APIEngine(lib);
class Url {
  constructor(lib) {
    this.lib = lib;
  }

  getThumbnailCache(trackHash, login) {
    return `${this.lib.config.host}/users/${login}/cache/cover/${trackHash}.jpeg`;
  }

  getTrackUrl(fileName, login) {
    return `${this.lib.config.host}/users/${login}/tracks/${fileName}`;
  }

  getThumbnailDefault() {
    return `${this.lib.config.host}/assets/music-default.png`;
  }

  getUserAvatar(token) {
    return `${this.lib.config.host}/assets/avatars/${token}.jpg`;
  }
}

module.exports = (lib) => new Url(lib);

class Parser {
  tryParseJSON(str) {
    if (typeof str !== 'string') return false;
    try {
      let jsonObj = JSON.parse(str);
      return jsonObj;
    } catch (err) {
      return false;
    }
  }
}

module.exports = new Parser();

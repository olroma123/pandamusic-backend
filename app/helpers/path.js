class Path {
  static getUserFolderPath(login) {
    return `./app/users/${login}`;
  }

  static getUserCacheFolderPath(login) {
    return `./app/users/${login}/cache`;
  }

  static getAssetsFolderPath() {
    return `./app/assets`;
  }

  static getAvatarFolderPath() {
    return `${this.getAssetsFolderPath()}/avatars`;
  }

  static getDefaultMusicCoverPath() {
    return `${this.getAssetsFolderPath()}/music-default.png`;
  }
}

module.exports = Path;

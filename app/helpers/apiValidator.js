class APIValidator {

  /**
   * Getter for popular regex to testing a value
   * @returns { Object } Returns an object with regex value with keys
   */
  static getRegexValidatorValues() {
    return {
      login: /[^-a-zA-Z0-9]/i,
      cyrilicOnly: /[^-а-яА-Я]/i
    }
  }

  /**
   * Method for validation params on each method by passing a name of param
   * @param { array } params An array with keys for validating
   * @param { object } data An object with requested param
  */
  static validateParamsDependingOnMethod(params, data) {
    if (!data) return false;
    return params.every((element, index) => data.hasOwnProperty(element) && data[element]);
  }

  /**
   * Method for validate a login of user by regex
   * @param { string } value User's login
   * @returns { boolean } Returns true if login is correct
   */
  static validateLoginRegex(value) {
    return !this.getRegexValidatorValues().login.test(value);
  }

  /**
   * Method for validate a login of user by regex
   * @param { string } value User's login
   * @returns { boolean } Returns true if login is correct
   */
  static validateCyrilicRegex(value) {
    return !this.getRegexValidatorValues().cyrilicOnly.test(value);
  }
}

module.exports = APIValidator;

module.exports = (lib) => {
	return {
		parser: require('./parser'),
		apiValidator: require('./apiValidator'),
		schema: require('./schema')(lib),
		path: require('./path'),
		url: require('./url')(lib)
	}
};